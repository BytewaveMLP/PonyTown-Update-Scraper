# PonyTown Update Scraper

> Scrapes the Pony Town sites for updates to the game's code, pushing them to Discord

PTUS is a very hacky Selenium/Chromedriver-based tool for periodically scraping Pony Town site updates, and posting information about them to Discord servers of your choosing via webhook URLs.

PTUS uses Chromedriver as Pony Town is a single-page web application behind Cloudflare. As a result, there's no easy way to simply extract the page contents with a service like `curl`; even if you use something that could defeat Cloudflare, you still need a JS execution environment to load the actual page. Parsing the JS file itself is difficult due to its size and irregularity, and is therefore infeasable.

## Installation

### Prerequisites

- Python 3
- `pip`
- An installation of Google Chrome, and a download of Chromedriver

### Setup

**A recommendation:** Don't run this on your local machine full-time. Offload it to a server somewhere. It'll be harder to trace back to you that way.

1. (optional) Create a `virtualenv` for your installation to avoid polluting your global package space. Use `virtualenv .env` in the directory where you've cloned PTUS to create one (you can install `virtualenv` with `sudo -H pip install virtualenv`). Once you've created a `virtualenv`, enter it with `source .env/bin/activate` (or `.\.env\Scripts\activate` on Windows).
2. `pip install -r requirements.txt`
3. `cp config.example.json config.json`
4. `$EDITOR config.json` (see [Configuration](#configuration) for details)
5. Test your installation by running `python main.py`. If you get notifications in your Discord servers, you're all set.
6. (optional, but highly recommended) Install a systemd timer (or equivalent on your platform) to run the script periodically. This script doesn't run in a loop; it's a one-shot application that is intended to be automated externally. See [Example systemd units](#example-systemd-units) for some of the ones I've written.

## Configuration

### `discord`

- `webhook_urls` - A list of webhook URLs (created in Discord) for channels you would like main site (`pony.town`) updates pushed to. This will include public version changes (accompanied by patch notes) as well as patchnoteless revision changes (ie, silent bug fixes).
- `beta_webhook_urls` - A list of webhook URLs for channels you would like beta site (`beta.pony.town`) updates pushed to. These will all be patchless revision changes, as the beta site does not provide change logs.
- `username` - The webhook usernmae to use for public site version changes
- `username_silent` - The webhook username to use for public site revision changes
- `username_beta` - The webhook username to use with beta site revision changes
- `avatar_url` - The avatar URL to use for the webhook

### `selenium`

- `chrome_path` - The path to your Chrome exectuable. On Linux, this can generally be found in `/usr/bin/google-chrome`, and on Windows, `C:\Program Files (x86)\Google\Chrome\Application\chrome.exe`.
- `chromedriver_path` - The path to your Chromedriver executable. This will be wherever Chromedriver was downloaded/installed to.

## Example systemd units

`ponytown-update-scraper.service` (no `virtualenv`):  
```ini
[Unit]
Description=Pony Town Update Scraper

[Service]
WorkingDirectory=/path/to/ponytown-update-scraper/
User=user
Group=group
ExecStart=/usr/bin/python3 /path/to/ponytown-update-scraper/main.py
Type=simple
StandardInput=null
StandardOutput=journal
Restart=on-failure
RestartPreventExitStatus=1
```

`ponytown-update-scraper.service` (`virtualenv`):  
```ini
[Unit]
Description=Pony Town Update Scraper

[Service]
WorkingDirectory=/path/to/ponytown-update-scraper/
User=user
Group=group
ExecStart=/path/to/ponytown-update-scraper/.env/bin/python3 /path/to/ponytown-update-scraper/main.py --serve-in-foreground
Type=simple
StandardInput=null
StandardOutput=journal
Restart=on-failure
RestartPreventExitStatus=1
```

`ponytown-update-scraper.timer` (**IMPORTANT:** this is what actually does the autmoation; make sure ot name it the same as your `.service` file)
```ini
[Unit]
Description=Pony Town Update Scraper (every 5min)

[Timer]
OnBootSec=5min
OnUnitActiveSec=5min

[Install]
WantedBy=multi-user.target
```

## Contributing

Feel free to contribute. Shoot merge requests, issues, whatever you may have.

## License

Copyright (c) 2017-19, Eliot Partridge. Licensed under [the MIT License](/LICENSE).

Pony Town is (c) Agamnentzar.