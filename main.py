import json
import os
import time
import requests
import jsoncfg
import sys

from bs4 import BeautifulSoup
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import TimeoutException

if not os.path.isfile('config.json'):
	print("It looks like you haven't set up your config yet!", file=sys.stderr)
	print("Please copy 'config.example.json' to 'config.json' and edit it accordingly before starting me.", file=sys.stderr)
	exit(1)

config = jsoncfg.load_config('config.json')

chrome_options = Options()
chrome_options.add_argument('--headless')
chrome_options.binary_location = config.selenium.chrome_path('/usr/bin/google-chrome-stable')

driver = webdriver.Chrome(
	executable_path=config.selenium.chromedriver_path('/usr/bin/chromedriver'),
	chrome_options=chrome_options
)

lastGameVersion = 'Unknown'
lastImageRev = 'Unknown'
lastBootstrapRev = 'Unknown'
lastBetaBootstrapRev = 'Unknown'
lastBetaGameVersion = 'Unknown'

if os.path.isfile('.lastversion'):
	with open('.lastversion', 'r') as f:
		lastGameVersion = f.read()

if os.path.isfile('.lastimagerev'):
	with open('.lastimagerev', 'r') as f:
		lastImageRev = f.read()

if os.path.isfile('.lastbootstrap'):
	with open('.lastbootstrap', 'r') as f:
		lastBootstrapRev = f.read()

if os.path.isfile('.lastbetaversion'):
	with open('.lastbetaversion', 'r') as f:
		lastBetaGameVersion = f.read()

if os.path.isfile('.lastbetabootstrap'):
	with open('.lastbetabootstrap', 'r') as f:
		lastBetaBootstrapRev = f.read()

print('Requesting public page...')
driver.get('https://pony.town/about')

try:
	print('  Waiting up to 10 seconds for the about page to load...')
	about = WebDriverWait(driver, 10).until(
		EC.presence_of_element_located((By.CSS_SELECTOR, 'about div.changelog-container'))
	)

	print('  Grabbing bootstrap script revision...')
	bootstrapRev = ''
	scripts = driver.find_elements_by_tag_name('script')
	for script in scripts:
		src = script.get_attribute('src')
		if src.startswith('https://pony.town/assets/scripts/bootstrap-'):
			print('    Found bootstrap script.')
			bootstrapRev = src.split('-')[1].split('.')[0]

	if bootstrapRev == '':
		print('    Failed to grab bootstrap revision.', file=sys.stderr)

	print('  Parsing updates list...')
	soup = BeautifulSoup(about.get_attribute('innerHTML'), 'html.parser')
	latestUpdate = soup.select_one('div')

	while latestUpdate.name != 'div' or not hasattr(latestUpdate, 'h4') or latestUpdate.h4 == None:
		latestUpdate = latestUpdate.nextSibling

	latestUpdateVer = latestUpdate.h4.text

	if latestUpdateVer != lastGameVersion:
		print('    New public update: ' + lastGameVersion + ' -> ' + latestUpdateVer)
		updateNotes = latestUpdate.ul.find_all('li')

		content = 'New public update: ' + lastGameVersion + ' -> ' + latestUpdateVer + '\n```md\n'

		for note in updateNotes:
			print('      -', note.text)
			content += '- ' + note.text + '\n'

		content += '```'

		imageBoxStyle = soup.select_one('div.image-box')['style']
		imagePath = imageBoxStyle[imageBoxStyle.index('("'):imageBoxStyle.index('")')].strip('()"')
		latestImageRev = imagePath.split('-')[1].split('.')[0]
		imageData = None

		if latestImageRev != lastImageRev:
			print(f'    New banner image: {lastImageRev} -> {latestImageRev}')
			imageRequest = requests.get(f'https://pony.town{imagePath}', headers={
				'Referrer': 'https://pony.town/about',
				'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.212 Safari/537.36'
			})
			if imageRequest.status_code == 200:
				print('Got image.')
				imageData = { 'file': (os.path.basename(imagePath), imageRequest.content, 'image/png') }
				with open('.lastimagerev', 'w') as f:
					f.write(latestImageRev)
			else:
				print(f'Failed to get image! Status: {imageRequest.status_code}')
		else:
			print('    No new image. Latest: ' + lastImageRev)

		for webhook_url in config.discord.webhook_urls:
			requests.post(webhook_url(), data={
				'username': config.discord.username('Pony Town Update'),
				'avatar_url': config.discord.avatar_url('https://i.imgur.com/SdyaP7U.png'),
				'content': content,
			}, files=imageData)

		with open('.lastversion', 'w') as f:
			f.write(latestUpdateVer)
	else:
		print('    No new updates. Latest: ' + lastGameVersion)

	if bootstrapRev != lastBootstrapRev:
		print('  New site script revision: ' + lastBootstrapRev + ' -> ' + bootstrapRev)

		content = 'New public bootstrap.js revision:\n'
		content += '**Old**: `' + lastBootstrapRev + '`\n'
		content += '**New**: `' + bootstrapRev + '`'

		for webhook_url in config.discord.webhook_urls:
			requests.post(webhook_url(), json={
				'username': config.discord.username('Pony Town Update'),
				'avatar_url': config.discord.avatar_url('https://i.imgur.com/SdyaP7U.png'),
				'content': content
			})

		with open('.lastbootstrap', 'w') as f:
			f.write(bootstrapRev)
	else:
		print('  No new site revision. Latest: ' + lastBootstrapRev)
except TimeoutException:
	print('Timed out waiting for public site to load.', file=sys.stderr)

print('Requesting beta page...')
driver.get('https://beta.pony.town/about')
try:
	print('Waiting up to 10 seconds for the about page to load...')
	version = WebDriverWait(driver, 10).until(
		EC.presence_of_element_located((By.CSS_SELECTOR, 'body > pony-town-app > div.container > footer > div.app-version > b'))
	)
	latestBetaUpdateVer = version.text

	print('  Grabbing bootstrap script revision...')
	bootstrapRev = ''
	scripts = driver.find_elements_by_tag_name('script')
	for script in scripts:
		src = script.get_attribute('src')
		if src.startswith('https://beta.pony.town/assets/scripts/bootstrap-'):
			print('    Found bootstrap script.')
			bootstrapRev = src.split('-')[1].split('.')[0]

	if bootstrapRev == '':
		print('    Failed to grab bootstrap revision.', file=sys.stderr)

	if lastBetaGameVersion != latestBetaUpdateVer and latestBetaUpdateVer.strip() != '':
		print('  New beta update: ' + lastBetaGameVersion + ' -> ' + latestBetaUpdateVer)
		content = 'New beta update: ' + lastBetaGameVersion + ' -> ' + latestBetaUpdateVer

		for webhook_url in config.discord.webhook_urls:
			requests.post(webhook_url(), json={
				'username': config.discord.username_beta('Pony Town Beta Update'),
				'avatar_url': config.discord.avatar_url('https://i.imgur.com/SdyaP7U.png'),
				'content': content
			})

		with open('.lastbetaversion', 'w') as f:
			f.write(latestBetaUpdateVer)
	else:
		print('  No new beta updates. Latest: ' + lastBetaGameVersion)

	if bootstrapRev != lastBetaBootstrapRev:
		print('  New beta site script revision: ' + lastBetaBootstrapRev + ' -> ' + bootstrapRev)

		content = 'New beta bootstrap.js revision:\n'
		content += '**Old**: `' + lastBetaBootstrapRev + '`\n'
		content += '**New**: `' + bootstrapRev + '`'

		for webhook_url in config.discord.beta_webhook_urls:
			requests.post(webhook_url(), json={
				'username': config.discord.username_beta('Pony Town Beta Update'),
				'avatar_url': config.discord.avatar_url('https://i.imgur.com/SdyaP7U.png'),
				'content': content
			})

		with open('.lastbetabootstrap', 'w') as f:
			f.write(bootstrapRev)
	else:
		print('  No new beta site revision. Latest: ' + lastBetaBootstrapRev)
except TimeoutException:
	print('Timed out waiting for beta site to load.', file=sys.stderr)
finally:
	driver.quit()
